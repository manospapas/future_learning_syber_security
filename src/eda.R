# setwd(config$project_path)

library('ProjectTemplate')
load.project()

# This function caluclates the total enrollments for each run.
# Parameters::
# Returns::
#   A list that contains the total enrollments of each run and the student ids who enrolled for each run.
# Usage::
#  enrollments()$numbers, enrollments()$ids[[1]][[1]], enrollments()$ids
enrollments = function(){
  
  numbers = ids = c()

  for(i in 1:length(config$enrollment_files)){
    
    data = datasets[[config$enrollment_files[i]]]
    
    numbers[i] = nrow(data) 
    
    ids[i]     = list(data$learner_id)
  }

  return(list(numbers = numbers, ids = ids))
}

# This function caluclates the total unenrollments for each run.
# Parameters::
# Returns::
#   A list that contains the total unenrollments of each run and the student ids who unenrolled for each run.
# Usage::
#  unenrollments()$numbers
unenrollments = function(){
  
  numbers = ids = c()
  
  for(i in 1:length(config$enrollment_files)){
    
    data = datasets[[config$enrollment_files[i]]]
    
    numbers[i] = nrow(data %>% filter(unenrolled_at > 0))
    
    ids[i] = list(data$learner_id)
  }
  
  return(list(numbers = numbers, ids = ids))
}

# This function caluclates the total order of certifications for each run.
# Parameters::
# Returns::
#   A list that contains the total orders of certification of each run and the students ids who bought a certification for each run.
# Usage::
#  purchased_statement()$ids
purchased_statement = function(){
  
  numbers = ids = c()
  
  for(i in 1:length(config$enrollment_files)){
    
    data = datasets[[config$enrollment_files[i]]] %>% filter(purchased_statement_at > 0)
    
    numbers[i] = nrow(data)
    
    ids[i] = select(data, learner_id)
  }

  return(list(numbers = numbers, ids = ids))
}

# This function caluclates the unenrollments per country. By default it will return the average.
# Parameters::
#   method (Method name) : The method name
# Returns::
#   A list, as a dataframe, that contains two columns. The first column is the countries 
#   and the second column is the result of the method that is applied.
# Usage::
#  unenrollment_per_country(sum)
unenrollment_per_country = function(method = mean){
  
  data = c()
  
  for(file in config$enrollment_files){
    
    data = rbind(data,datasets[[file]] %>%
                   
      select(unenrolled_at,detected_country) %>%
      
      filter(unenrolled_at > 0 & !is.na(detected_country)) %>%
      
      group_by(detected_country) %>%
      
      summarise(count = n()))
  }
  
  data = as.data.frame(data %>% group_by(detected_country) %>% summarise(method = method(count)))
  
  return(data)
}

# This function finds the enrollment details of the people who bought a certification.
# Parameters::
# Returns::
#   A list, as a dataframe, that contains all the buyers.
# Usage::
#  stats_of_buyers()
stats_of_buyers = function(){
  
  stats = c()
  
  for(file in config$enrollment_files){
    
    data = datasets[[file]]
    
    stats = rbind(stats, data %>% filter(purchased_statement_at > 0))
  }
  
  return(stats) 
}

# This function finds the enrollment details of the people who bought a certification in different files.
#   By default the function merges the enrollment_files with itself.
# Parameters::
# Returns::
#   A list, as a dataframe, that contains all the buyers with their details. The information
#   that are available are the columns of enrollment_files and the given files.
# Usage::
#  stats_of_buyers()
buyers_extra_info = function(relative_files = config$enrollment_files){
  
  buyers = c()
  
  for(i in 1:length(relative_files)){
    
    current_buyers = read_similar_files(relative_files[i]) %>% filter(purchased_statement_at > 0)
                              
    find_buyers = merge(stats_of_buyers(), current_buyers, by="learner_id", suffixes = "")
    
    buyers = rbind(buyers, find_buyers)
  }
    
  return(buyers)
}

# This function plots the enrollments and the unenrollments for each run.
# Parameters::
#   figure (String) : The figure that the image will take. By default it is empty.
# Returns::
#   A barplot.
# Usage::
#  plot_enrollments("Figure 1")
plot_enrollments = function(figure = ""){

  theme_set(theme_bw())
  
  g = ggplot(all_enrollments, aes(run))
  
  status = ifelse(is.na(all_enrollments$unenrolled_at), 'Stay', 'Left')
  
  plot = g + geom_bar(aes(fill = status)) +
    scale_fill_manual("legend", values = c("Left" = "red", "Stay" = "blue")) +
    ggtitle("(Un)Enrolments over different runs \n (with NA)") +
    xlab(paste("Runs \n\n", figure, sep = "")) +
    ylab("Count of (un)enrolments")
  
  # Causes an error if the project is not laoded and the variables are not in the enviroment.
  tryCatch(ggsave(file.path('graphs', '(Un)Enrolments_over_different_runs.pdf')), error=function(e){})
  
  return(plot)
}

# This function plots the enrollments and the unenrollments for each run.
# Parameters::
#   figure (String) : The figure that the image will take. By default it is empty.
# Returns::
#   A barplot.
# Usage::
#  plot_age_groups("Figure 1")
plot_age_groups = function(figure = ""){

  # Remove the rows that do not contribute.
  enrollments_without_na = all_enrollments %>% filter(
    age_range != "Unknown" & country != "Unknown" & gender != "Unknown" & employment_status != "Unknown" & highest_education_level != "Unknown")
  
  g = ggplot(enrollments_without_na, aes(run))
  
  plot = g + geom_bar(aes(fill = age_range)) +
    ggtitle("Age groups over different runs (without NA)") +
    xlab(paste("Runs \n\n", figure, sep = "")) +
    ylab("Count of age groups")
  
  # Causes an error if the project is not laoded and the variables are not in the enviroment.
  tryCatch(ggsave(file.path('graphs', 'Age_groups_over_different_runs.pdf')), error=function(e){})
  
  return(plot)
}

# This function plots the countries with the most students.
# Parameters::
#   nof_countries (Integer) : The number of countries we want to take. By default it will return 10.
#   figure       (String)   : The figure that the image will take. By default it is empty.
# Returns::
#   A plot with the N top selected countries.
# Usage::
#  plot_top_countries(12, "Figure 1")
plot_top_countries = function(nof_countries = 10, figure = ""){

  countries = all_enrollments %>% group_by(detected_country) %>% count
  
  top_counties = top_n(as.data.frame(countries), nof_countries, n)
  
  g = ggplot(top_counties, aes(detected_country))
  
  plot = g +  geom_point(aes(y = n / length(config$enrollment_files))) +
    ggtitle(paste("Top ", nof_countries ," countries (with NA)", sep = "")) +
    xlab(paste("Countries \n\n", figure, sep = "")) +
    ylab("Average count of enrolments")
  
  # Causes an error if the project is not laoded and the variables are not in the enviroment.
  tryCatch(ggsave(file.path('graphs', paste('Top_', nof_countries ,'_countries.pdf', sep = ""))), error=function(e){})
  
  return(plot)
}

# This function plots a pie with the step completion as a percentage.
# Parameters::
#   figure  (String) : The figure that the image will take. By default it is empty.
# Returns::
#   A pie that contains the percentage of the step completion.
# Usage::
#  plot_step_completion("Figure 1")
plot_step_completion = function(figure = ""){

  users_steps = week_activity %>% group_by(learner_id) %>% count
  
  # We assume that the steps are the same. Maximum is 62.
  users_all_steps = users_steps %>% filter(n == 62)
  
  users_q3_steps = users_steps %>% filter(n >= 47 & n <62)
  
  users_q2_steps = users_steps %>% filter(n >= 31 & n < 47)
  
  users_q1_steps = users_steps %>% filter(n >= 16 & n < 31)
  
  users_q0_steps = users_steps %>% filter(n <16)
  
  quantile_steps = c(
    nrow(users_all_steps) / nrow(users_steps), 
    nrow(users_q3_steps) / nrow(users_steps), 
    nrow(users_q2_steps) / nrow(users_steps), 
    nrow(users_q1_steps) / nrow(users_steps), 
    nrow(users_q0_steps) / nrow(users_steps))
  
  steps_frame = data.frame(group = c("100%", ">75%", "50%-75%", "25%-50%", "0%-25%"), value = quantile_steps)
  
  
  bp = ggplot(steps_frame, aes(x = "", y = value, fill = group)) + geom_bar(width = 1, stat = "identity")
  
  plot = bp + coord_polar("y", start=0) + 
    ggtitle("Percentage of step completion") + 
    xlab("") +
    ylab(figure)
  
  # Causes an error if the project is not laoded and the variables are not in the enviroment.
  tryCatch(ggsave(file.path('graphs', 'Step_Completion.pdf')), error=function(e){})

  return(plot)
}
