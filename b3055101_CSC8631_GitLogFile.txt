commit df955c680465e4d7652f4cf75dcbfa7a3fbf4d8b
Merge: 6b5c8ac df67e5f
Author: Manos Papas <e.papagiannidis@gmail.com>
Date:   Thu Nov 22 09:02:07 2018 +0000

    Merge branch 'Development' into 'master'
    
    Development
    
    See merge request manospapas/future_learning_syber_security!2

commit df67e5f398e2ebe02ecc53a79b8e6280bc169e5c
Merge: 49d5e17 6b5c8ac
Author: Manos Papas <e.papagiannidis@gmail.com>
Date:   Thu Nov 22 09:01:35 2018 +0000

    Merge branch 'master' into 'Development'
    
    # Conflicts:
    #   config/global.dcf
    #   lib/globals.R
    #   lib/helpers.R
    #   munge/01-A.R
    #   reports/future_learning.Rmd
    #   src/eda.R
    #   tests/1.R

commit 49d5e170c196e892147a21dfd97b857c56d658dd
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Thu Nov 22 08:53:59 2018 +0000

    Add Read_me.txt
    
    A walkthrough the project

commit ee66e38d98757a6be303999b9c586e33e720058c
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Thu Nov 22 08:53:19 2018 +0000

    Add graphs and test_folder

commit 5a41655f59c63bbd58fce857895caf11f83d86bb
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Thu Nov 22 08:52:55 2018 +0000

    Add cache for speed

commit 13b3b2cc2fdb936a68446b2f9f50cc75014bfa35
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Thu Nov 22 08:52:10 2018 +0000

    Add extra tests

commit 06b44879cb434eb8bebe57211e9188ca50750ad9
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Thu Nov 22 01:05:40 2018 +0000

    Add cache for all_enrollments to improve speed

commit b92df50cf210183ce4247b562ae7ec50b0c79578
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Thu Nov 22 01:04:48 2018 +0000

    Add ggsave to save the plots

commit a6d82cba6a7e4d1840861a1bbc47e8c3fecf2e83
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Thu Nov 22 01:04:08 2018 +0000

    Imporve function extract_number

commit 4fcfe771ec6605e5101bc06b573a94e3e90a225e
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Wed Nov 21 23:08:22 2018 +0000

    Minor fixes in reports

commit 2f2824e6fa72cb396b7dc68f5ba15dc3ded7de65
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Wed Nov 21 22:00:19 2018 +0000

    Add configuration - libraries

commit 17cee420c15c6c1012c79b7d4a6f3687195578cd
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Wed Nov 21 21:57:51 2018 +0000

    Add R markdown report

commit 0dbd1e58515f27a7a33140f8bda2549ee335453f
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Wed Nov 21 07:01:23 2018 +0000

    Add R Markdown in Reports
    
    The R Markdown for CRISP_DM. Also a minor fix in eda.R.

commit bae8e255084b32243927512eb3d5ea347cc0bf87
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Wed Nov 21 06:58:22 2018 +0000

    Add extra libraries and improve lib

commit 4069010d4c0920aec9a645f228fcf4b00cc82655
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 23:01:15 2018 +0000

    Fix bug for project_path

commit 830395b3f2ab80641c832bf29095ef08cfe9bcef
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 22:09:44 2018 +0000

    Add dynamic project path

commit dc9e30316f3d56e1675279a6be3be94d7378b260
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 21:00:35 2018 +0000

    Add more test
    
    for summarise_data, read_similar_files, column_method methods

commit 4f2d1c5003c2920a4115b6700c9c2a7d8bfa184b
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 20:59:51 2018 +0000

    Add missing files
    
    from the raw data

commit e2d16c1f6b14f745d851306e5344ee67dfa0ba2f
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 20:48:09 2018 +0000

    Remove folder dataset
    
    The dataset folder is now raw_data

commit 6b5c8ac55eaf49cbfbfab26251fb663504642b96
Merge: 0901039 8ae63d1
Author: Manos Papas <e.papagiannidis@gmail.com>
Date:   Mon Nov 19 09:06:35 2018 +0000

    Merge branch 'Development' into 'master'
    
    Development
    
    See merge request manospapas/future_learning_syber_security!1

commit 8ae63d140e3e26b891c227662c64c65281f8366d
Author: Manos Papas <e.papagiannidis@gmail.com>
Date:   Mon Nov 19 09:06:33 2018 +0000

    Development

commit 9ccdba6ccee4a5429a4ed94f18665c8415c5767e
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:30:10 2018 +0000

    Revert "Remove csv files"
    
    This reverts commit 5434b2d3002fa2159d4f83b8ab96d1c9463ec25c.

commit 105ce690fb1cb98b878c72e876f18ab5f79ca4ad
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:35:50 2018 +0000

    Upload raw files

commit 0f4910ba313d8d424cba2d3a9c14f887e6c72297
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:30:37 2018 +0000

    Revert "Revert "Remove csv files""
    
    This reverts commit 68522ba3de8129455322a88efd7db1b14482b304.

commit 9a0267ad6cf6047ae6ded6e89db679cebe385ec2
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:30:27 2018 +0000

    Remove extra file

commit 68522ba3de8129455322a88efd7db1b14482b304
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:30:10 2018 +0000

    Revert "Remove csv files"
    
    This reverts commit 5434b2d3002fa2159d4f83b8ab96d1c9463ec25c.

commit 5434b2d3002fa2159d4f83b8ab96d1c9463ec25c
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:29:52 2018 +0000

    Remove csv files
    
    and add test_folder

commit 8108ee7ac02ecb46c44a4e8d8581587261860878
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:27:58 2018 +0000

    Revert "Revert "Revert "Add cache and minor change in global configuration"""
    
    This reverts commit d2c5b8634354d9fcee472ef56bf0973993cb45a3.

commit d2c5b8634354d9fcee472ef56bf0973993cb45a3
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:27:34 2018 +0000

    Revert "Revert "Add cache and minor change in global configuration""
    
    This reverts commit d135c799659a3f3e6824e7af8fc25fa0d426c9b2.

commit d135c799659a3f3e6824e7af8fc25fa0d426c9b2
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:27:13 2018 +0000

    Revert "Add cache and minor change in global configuration"
    
    This reverts commit 19d8e71503bce6bd3ce5cb1d7338f1dd12408bf6.

commit 19d8e71503bce6bd3ce5cb1d7338f1dd12408bf6
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:25:57 2018 +0000

    Add cache and minor change in global configuration
    
    Change the libraries. The cache is for speed.

commit ce189d58e7d4b5284f0852015502fb4df48424da
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:24:51 2018 +0000

    Add tests for helper methods

commit cc116554e2b81af863aecc967afccc4b0d00474b
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:24:24 2018 +0000

    Improve scr and remove containt from munge
    
    Add documentation and fix bugs

commit 5fc59b46b32810d5ee5f37568a7f570ef437c74b
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Mon Nov 19 01:20:45 2018 +0000

    Improve helpers and globals
    
    Add functionality, documentation and  fix bugs.

commit ff9b7bf0f2704b7ea7398385fab990de396a3c7e
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Wed Nov 14 23:37:43 2018 +0000

    Minor fixes in munge

commit 2301f7bbed3d737dd82b9894bf7ec6cdb7954729
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Wed Nov 14 23:36:07 2018 +0000

    Add extra fuctionality

commit 60ec1ccd512df9772610e54b5a188804e9e97f8d
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Wed Nov 14 23:35:39 2018 +0000

    Add report
    
    For the analysis

commit 41aeeebcfc3cb50d3d2ac1677ddbc3eb3555d99d
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Wed Nov 14 23:34:48 2018 +0000

    Change configuration

commit 5176377116da62b929d16b2f1076dfe5c9ab0c2d
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Sat Nov 10 16:29:16 2018 +0000

    Load and write data
    
    Read raw data and add NA values. Moreover, write the new files to another folder, so that raw data are not affected.

commit 24579bf814bfa46769320897f96d06e6ea18dde5
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Sat Nov 10 16:27:44 2018 +0000

    Add configuration

commit 7621e21bcc4365f05340769b84de92f30e95a3f0
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Tue Nov 6 22:23:42 2018 +0000

    Add datasets

commit 090103972a864b8a76bbee832f42c8ab0229fe33
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Tue Nov 6 22:06:25 2018 +0000

    Change file destination

commit f70707b3b234f739011701301a4922aa13f026f8
Author: EmmanouilPapagiannidis <e.papagiannidis@ac.uk>
Date:   Tue Nov 6 21:41:43 2018 +0000

    Initial commit
