# This function loads the datasets.
# Parameters::
#   files_path    (String) : The file path of the files. By default it will take "./data/clean_data/".
#   files_pattern (String) : The file pattern of the files. By default it will take the "*.csv".
# Returns::
#   A list of datasets. 
# Usage::
#   variable = load_data()
#   variable[[file_name]]$column_name
load_data = function(files_path = config$clean_data_path, files_pattern = config$file_pattern){
  
  file_names = list.files(pattern = files_pattern, path = files_path)
  
  files = c()
  
  for(file_name in file_names){
    
    temp_file = fread(paste(files_path, file_name, sep = ""), na.strings=c("", "NA"))
    
    # Remember in which run the data came.
    if(files_path == config$raw_data_path)
      temp_file = temp_file %>% mutate(run = extract_number(file_name))
    
    files[file_name] = list(temp_file)
  }
    
  
  return(files)
}

# This function stores the data.
# Parameters::
#   datasets        (List of datasets) : The datasets, which are going to be stored.
#   files_path      (String)           : The file path of the files. By default it will take "./data/clean_data/".
#   folder_name     (String)           : The folder name that the files will be stored. By default it will take the "clean_data".
#   files_store_path(String)           : The file path of the files. By default it will take "./data/clean_data/".
#   files_pattern   (String)           : The file pattern of the files. By default it will take the "*.csv".
# Returns::
#   Nothing. 
# Usage::
#   store_data()
store_data = function(datasets, files_path = config$datasets_folder, folder_name = config$clean_folder, 
                      files_store_path = config$raw_data_path, files_pattern = config$file_pattern){
  
  file_names = list.files(pattern = files_pattern, path = files_store_path)
  
  # It does not override.
  dir.create(file.path(files_path, folder_name), showWarnings = FALSE)
  
  # Change working directory to write files.
  setwd(file.path(files_path, folder_name))
  
  for(i in 1:length(file_names))
    write.csv(datasets[[i]], file = file_names[i], row.names = FALSE)
  
  setwd(config$project_path)
}

# This function fixes a bug. Combined columns that contains numbers and seperate by a '.' give a double instead of a string.
# For example: combine 1 and 10 will give 1.1 instead of 1.10
clean_data = function(datasets){
  
  for(file in config$leaving_files)
    datasets[[file]] = datasets[[file]] %>% 
      mutate(last_completed_step = ifelse((is.na(last_completed_step_number) | is.na(last_completed_week_number)),
                                    NA, paste(last_completed_week_number, last_completed_step_number, sep = ":")))
  
  for(file in config$question_files)
    datasets[[file]] = datasets[[file]] %>% 
      mutate(quiz_question = ifelse((is.na(week_number) | is.na(step_number) | is.na(question_number)), 
                              NA, paste(week_number, step_number, question_number, sep = ":")))
  
  for(file in config$activity_files)
    datasets[[file]] = datasets[[file]] %>% 
      mutate(step = ifelse((is.na(week_number) | is.na(step_number)), 
                      NA, paste(week_number, step_number, sep = ":")))
  
  return(datasets)
}

# This function summarise the given column for all relative files.
# Parameters::
#   relative_files (List of Strings): the relative files. You can find them in the config file.
#   column         (String)         : the column name to group_by.
# Returns::
#   A list that contains a dataframe for each file that has two columns. The first column is the given column and the second is the counts of each group.
# Usage::
#   summarise_column(config$enrollment_files, "detected_country")[[1]]$count > 10
summarise_column = function(relative_files, column){
  
  table = list()
  
  for(i in 1:length(relative_files))
    table[i] = list(as.data.frame(datasets[[relative_files[i]]] %>% group_by_(.dots = column) %>% summarise(count = n())))
  
  return(table)
}

# This function reads similar files and combines them into one.
# Parameters::
#   relative_files (List of Strings): the relative files. You can find them in the config file.
# Returns::
#   A list, as data frame attaching all relative files's content into one.
# Usage::
#   read_similar_files(config$enrollment_files)
read_similar_files = function(relative_files){
  
  combine_files = c()
  
  for(i in 1:config$nof_relative_files)
    combine_files = as.data.frame(rbind(combine_files, datasets[[relative_files[i]]]), stringsAsFactors=FALSE)
  
  return(combine_files)
}

# This function reads similar files, combines them into one and applying the given method. By default the average will be given.
# Parameters::
#   relative_files (List of Strings): the relative files. You can find them in the config file.
#   column         (String)         : The column that is the method will apply.
#   methpd         (Method name)    : The method name. 
# Returns::
#   A list, as data frame attaching all relative files's content into one. The list contains only the groups of the given column
#   as well as the result of the computation for each group.
# Usage::
#   column_method(config$enrollment_files, "detected_country", sum)
column_method = function(relative_files, column, method = mean){
  
  sum_status = summarise_column(relative_files, column)
  
  mean_status = c()
  
  for(i in 1:config$nof_relative_files)
    mean_status = rbind(mean_status, sum_status[[i]])  
  
   result = as.data.frame(mean_status %>% group_by_(.dots = column) %>% summarise(method = method(count)))
  
  return(result)
}

# This function reads a string and returns the number of that string.
# Parameters::
#   string (String) : The string we want to find the number
# Returns::
#   A vector of the integers that have been founded. Otherwise, it prints a warning and returns -1.
# Usage::
#   extract_number("d12a") => 12
extract_number = function(string){
  
  number = as.integer(unlist(regmatches(string,gregexpr("[[:digit:]]+\\.*[[:digit:]]*",string))))

  if(length(number) > 0)
    return(number)
  
  print("Warning: No number has been found!")
  
  return(-1)
}
